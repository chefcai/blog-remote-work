## Working Remotely vs. Remotely Working

With the whole world changing around us in such a short time a lot of people have found their living and working conditions vastly different.
For many, the collision occurring due to the Covid-19 global epidemic which blended and bled both our living and working conditions.

A lot of people who tote the benefits of working remotely often say that it produces a better work-life balance.
You don’t spend your life in a car, commuting, finding parking, stressing out about whether your pants are wrinkled, making it to that Monday morning meeting Chad still scheduled even though we have an unspoken rule about Monday mornings and Friday afternoons.
Many people prefer working in an office - it allows for clear “This is working time” v “This is not working time”
This blurring of lines is a double-edged sword - you’ll find people roll out of bed, fix their bed head, throw on a nice shirt and jump on their video call - or don’t take this step and dial in for their phone conference.
I’ve heard of people feeling the pinch of not seeing co-workers and feeling less productive. 
Almost everyone is trying to use video conferencing for a one-to-one swap for in-person meetings. 
This isn’t enough. 
Virtual meetings are completely different than in-person, both for attention and the ability to take action for everyone. 
Management needs to respect their employees time and adapt to these new conditions.

- Make the meeting shorter where possible
- get right to the point 
- Make a list of “Action Items” so that people walk away knowing what they were supposed to do and let the mental wander crush their day

It is up to the leader of a group, even if you’re not the manager, to find way to deal with it. Most importantly SHARE that success. 
  
We’ve implemented virtual coffee dates through our company collaboration tool
Slack (https://bit.ly/RACslack)
using Donut (https://bit.ly/RACdonut) 
Donut is a random meeting tool with people at work sidestepping having to set time aside and end up chatting with the same people you’re in a video conference half your work day.
We made this engagement optional and almost everyone in the company has taken to using it.
Donut also gives an icebreaker option so it isn’t just “Hey…” “Hi…” “Uhhh, yeah…”

There are other creative ways to connect with your workers.
Recently a co-worker showed us Drawful 2 (https://bit.ly/RACdrawful2) using our mobile devices (which let’s face it we all have) and adding video conference for the “guess-what your coworker drew” we were able to have a little recreation time that was hilarious and time capped without frustration.

What if you didn’t use that extra time for ZZzzs before being productive?
That doesn’t mean sleeping extra isn’t valid - but we all know the pain of Daylight Savings Time adjustments…
you aren’t going to keep that extra hour forever.
Does it make sense to roll out of bed at 9:15 for your 9:30 meeting.
Usually you’d be up at 7:30 getting ready for work - Try to stay in that routine: put hands on keyboard an hour ahead of time.
You SHOULD then use that extra time to take a longer lunch - get out from in front of the computer for virtual coffee breaks, go for a walk around the block (while maintaining a safe social distance and being respectful of others’ choices regarding that)
It is VERY important you continue to take those water breaks, get some coffee, and establish in a routine that doesn’t overwork OR cause underproduction.

This is a time to maximize that work-life balance. Really decide if “this is for you”.
Many industries have been forced to find ways of navigating work-from-home.
That could mean you get partial work-from-home benefits after returning.

We at RunAsCloud appreciate that flexibility and I’m sure you can find that beneficial too.

RunAsCloud has maintained our position on allowing for that flexibility.
Everyone of our employees has used time to Work-From-Home at some point prior to this unprecedented event.
This isn’t your boss cutting out from work early for golf, but any employee finding out 
their dental appointment couldn’t be scheduled for another time, 
one of our children has to be picked up early sick-from-school and we have one last part to a project - 
taking a full day off is going to shift our deadline or could oherwise cause coming into work while (even mildly) sick. 

RunAsCloud also maintains regular offices and hours to encourage in-person collaboration. 
Meeting our co-workers face-to-face, even when having a holiday party, beginning of the year ‘kick-off’,
or in-person meetings on a regular basis with work-from-home options has been a key part of making our team feel closer.
We are social creatures and being confined to our room / apartment for the past month has been hell for a lot of people. 

Work with your co-workers, boss, and family to navigate this time.

You aren’t alone. 

Don’t let that feeling hold you back from achieving what you are capable.